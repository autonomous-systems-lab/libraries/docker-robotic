# docker-robotic

This is the base repository for the robotic docker images. It includes images for commonly used linux software environments
whit AS team.

## Using the images

### Software requirements
- Docker: [install from here](https://docs.docker.com/engine/install/ubuntu/)
- Docker compose: [install from here](https://docs.docker.com/compose/install/)

### Pull / Run an image

After logging in, you can pull or run an image the usual way.

`docker run --rm -it registry.gitlab.com/autonomous-systems-lab/libraries/docker-robotic/ros-desktop:melodic-18.04 bash`

Please pick the image and tag (read: image version) and append that to the
registry.gitlab.com/autonomous-systems-lab/libraries/docker-robotic/<imagename>:<tag>. 
Or alternatively, you can use the **Copy button** on the Gitlab Container registry (see below).

Images are usually tagged with `<ROS Distribution>-<Ubuntu version>` if not specified otherwise. 
E.g. `noetic-20.04` for a ROS Noetic image based on Ubuntu 20.04. 

Here's a short description of the available images:

* **ros**: Base ROS installation including RViz etc (essentially `apt install ros-<distr>-desktop`)
* **franka**: ROS based image including MoveIt and franka_ros
* **realsense2** Intel realsence camera driver and ros sdk
* **kinect-azure** Kinnect azure camera driver and ros sdk
* **panda-simulator** Gazeebo based simulation environment for Franka Emika Panda robot based on this probject: https://github.com/justagist/panda_simulator

Please see the container registry page for a complete list of available images: 
https://gitlab.com/autonomous-systems-lab/libraries/docker-robotic/container_registry

### Sample project

Please take a look at out example project [examples/franka-robot-rviz-moveit](../examples/franka-robot-rviz-moveit )

You can run that using docker-compose.
First make sure that your environment setup is correct by changign the relevant variable at the top of the docker-compose.yml. Then start everythin by running
```
> docker-compose up 
```

Or you can start components individually using `docker-compose run <service>`

```
first terminal:
> docker-compose run roscore

second terminal:
> docker-compose run franka-robot
```



### Notes
Here's sample command that runs a rostopic echo on our franka image: 

`docker run --rm -it --net=host -e ROS_HOSTNAME=192.168.42.1 -e ROS_MASTER_URI=http://192.168.42.50:11311/ registry.gitlab.com/festo-research/teiresias/rlroboticgroup/docker-franka/franka:noetic-20.04 /main_env.sh rostopic echo -c /joint_states`
Please note following:
- the `.bashrc` and `.bash_profile` are **not** executed by default (only if you run bash as the container command). Therefore the relevant parts of those files needs to be done manually. This also means that you can't *source* an environment easily, since the enviroment variable changes from sourcing don't carry over to other commands.
- because workspaces can't be sourced, running ROS commands (roscore, rostopic, rosservice, rosrun, roslaunch etc.) requires that you prefix each command with the respective workspace' env.sh .
  `/main_env.sh` is a utility script that executes the `env.sh` from the image's main workspace (e.g. `/opt/ros/melodic` or `/franka_ros_ws`).
  So in the above command `/main_env.sh rostopic ...` essentially does the same as `source /opt/ros/noetic/setup.bash && rostopic ...`
- passing `ROS_HOSTNAME` and `ROS_MASTER_URI` environment variables is analogue to the ususal `export ....` commands you would execute in a shell (or in your .bashrc)
- `--net=host` makes the container use the host's network interfaces. That is a easy workaround for accessing other containers (potentially on other machines), but it is recommended to use a dedicated container network and hostnames (eg. via docker-compose) if possible.
