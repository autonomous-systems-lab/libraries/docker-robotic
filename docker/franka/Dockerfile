ARG BASEIMAGE_NAME=registry.gitlab.com/autonomous-systems-lab/libraries/docker-robotic/ros-desktop
ARG BASEIMAGE_TAG=noetic-20.04

FROM ${BASEIMAGE_NAME}:${BASEIMAGE_TAG}

ARG BASEIMAGE_ROS_DISTRO=noetic
ARG LIBFRANKA_BRANCH=master
ARG FRANKAROS_BRANCH=noetic-devel

RUN apt-get update

RUN apt-get install -y build-essential cmake git libpoco-dev libeigen3-dev


# Clone libfranka with changes to the realtime-kernel file
# and disable realtime-kernel check libfranka code
RUN git clone --recursive -b ${LIBFRANKA_BRANCH} https://github.com/frankaemika/libfranka && \
    sed -i '69d' libfranka/src/control_loop.cpp && \
    sed -i '69 s/;/;setCurrentThreadToHighestSchedulerPriority(\&error_message);/' libfranka/src/control_loop.cpp && \
    sed -i '70,75d' libfranka/src/control_loop.cpp

RUN mkdir -p libfranka/build
WORKDIR /libfranka/build
RUN cmake -DCMAKE_BUILD_TYPE=Release .. && \
    cmake --build .

# Copy Franka ROS
RUN apt-get install ros-${BASEIMAGE_ROS_DISTRO}-catkin && \
    mkdir -p /franka_ros_ws/src/ && \
    apt-get update && apt-get install -y \
        ros-${BASEIMAGE_ROS_DISTRO}-moveit \
        ros-${BASEIMAGE_ROS_DISTRO}-joint-trajectory-controller \
        ros-${BASEIMAGE_ROS_DISTRO}-moveit-visual-tools

# Install libfranka and build catkin
WORKDIR /franka_ros_ws
RUN . /opt/ros/${BASEIMAGE_ROS_DISTRO}/setup.sh && catkin_init_workspace src && \
    git clone --recursive -b ${FRANKAROS_BRANCH} https://github.com/frankaemika/franka_ros.git src/franka_ros && \
# RUN git clone -b noetic-devel https://github.com/ros-planning/panda_moveit_config.git src/panda_moveit_config \
    /main_env.sh rosdep install --from-paths src --ignore-src --rosdistro ${BASEIMAGE_ROS_DISTRO} -y --skip-keys libfranka && \
    /main_env.sh catkin build -DCMAKE_BUILD_TYPE=Release -DFranka_DIR:PATH=/../libfranka/build && \
    echo "source /franka_ros_ws/devel/setup.bash" >> ~/.bashrc && \
    echo "/franka_ros_ws/devel" > /etc/main_ros_workspace


ADD ./franka_tools/ /franka_ros_ws/src/franka_tools/
RUN chmod +x /franka_ros_ws/src/franka_tools/scripts/*.py && \
    catkin build

RUN . /franka_ros_ws/devel/setup.sh

#CMD ["tail", "-f", "/dev/null"]
