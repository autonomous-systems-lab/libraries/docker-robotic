#!/usr/bin/env python3

import argparse

import geometry_msgs.msg
import rospy
import tf.transformations

import moveit_commander
import yaml
from typing import Dict, Any


def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser()

    parser.add_argument(
        'file',
        help='The config file to load planning scene objects from.'
    )

    parser.add_argument(
        '--clear',
        action='store_true',
        help='Pass to clear the planning scene before loading a config.'
    )
    return parser


def load_pose(config: Dict[str, Any]) -> geometry_msgs.msg.Pose:

    res = geometry_msgs.msg.Pose()

    position = config['position']
    if isinstance(position, (list, tuple)):
        res.position = geometry_msgs.msg.Point(*position)

    elif isinstance(position, dict):
        res.position.x = position['x']
        res.position.y = position['y']
        res.position.z = position['z']

    else:
        raise ValueError('Unknown position type %s' % type(position))

    orientation = config.get('orientation')
    if orientation is None:
        q = tf.transformations.quaternion_about_axis(0, (1, 0, 0))
        res.orientation = geometry_msgs.msg.Quaternion(q[0], q[1], q[2], q[3])

    elif isinstance(orientation, (list, tuple)):
        res.orientation = geometry_msgs.msg.Quaternion(*orientation)

    elif isinstance(orientation, dict):
        res.orientation.x = orientation['x']
        res.orientation.y = orientation['y']
        res.orientation.z = orientation['z']
        res.orientation.w = orientation['w']

    else:
        raise ValueError('Unknown orientation type %s' % type(orientation))

    return res


def load_pose_stamped(config: Dict[str, Any]) -> geometry_msgs.msg.PoseStamped:
    res = geometry_msgs.msg.PoseStamped(pose=load_pose(config))
    res.header.frame_id = config.get('frame', 'world')
    return res


def add_sphere(config: Dict[str, Any], planning_scene: moveit_commander.PlanningSceneInterface):
    name: str = config['name']
    rospy.loginfo('Adding sphere %s' % name)

    pose = load_pose_stamped(config['pose'])
    radius = config.get('radius', 1)

    planning_scene.add_sphere(name, pose, radius)


def add_cylinder(config: Dict[str, Any], planning_scene: moveit_commander.PlanningSceneInterface):
    name: str = config['name']
    rospy.loginfo('Adding cylinder %s' % name)

    pose = load_pose_stamped(config['pose'])
    height = config.get('height', 1)
    radius = config.get('radius', 1)

    planning_scene.add_cylinder(name, pose, height, radius)


def add_mesh(config: Dict[str, Any], planning_scene: moveit_commander.PlanningSceneInterface):
    name: str = config['name']
    rospy.loginfo('Adding mesh %s' % name)

    pose = load_pose_stamped(config['pose'])
    filename = config['filename']
    size = tuple(config.get('size', [1, 1, 1]))

    planning_scene.add_mesh(name, pose, filename, size)


def add_box(config: Dict[str, Any], planning_scene: moveit_commander.PlanningSceneInterface):
    name: str = config['name']
    rospy.loginfo('Adding box %s' % name)

    pose = load_pose_stamped(config['pose'])
    size = tuple(config.get('size', [1, 1, 1]))

    planning_scene.add_box(name, pose, size)


def add_plane(config: Dict[str, Any], planning_scene: moveit_commander.PlanningSceneInterface):
    name: str = config['name']
    rospy.loginfo('Adding plane %s' % name)

    pose = load_pose_stamped(config['pose'])
    normal = tuple(config.get('normal', [0, 0, 1]))
    offset = config.get('offset', 0)

    planning_scene.add_plane(name, pose, normal, offset)


def main(args: argparse.Namespace, parser: argparse.ArgumentParser):
    with open(args.file, 'r') as fh:
        config = yaml.load(fh)

    planning_scene = moveit_commander.PlanningSceneInterface(synchronous=True)

    if args.clear:
        rospy.loginfo('Clearing existing planning scene')
        planning_scene.clear()

    for name, obj in config['objects'].items():
        t: str = obj['type'].lower()
        obj['name'] = name

        if t == 'sphere':
            add_sphere(obj, planning_scene)
        if t == 'cylinder':
            add_cylinder(obj, planning_scene)
        if t == 'mesh':
            add_mesh(obj, planning_scene)
        if t == 'box':
            add_box(obj, planning_scene)
        if t == 'plane':
            add_plane(obj, planning_scene)

    rospy.loginfo('Finished adding objects')


if __name__ == '__main__':
    rospy.init_node('planning_scene_setup')
    parser = create_parser()
    args = parser.parse_args()

    main(args, parser)

    #rospy.spin()

