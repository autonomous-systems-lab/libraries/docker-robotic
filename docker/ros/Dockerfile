ARG BASEIMAGE_NAME=ubuntu
ARG BASEIMAGE_TAG=20.04

FROM ${BASEIMAGE_NAME}:${BASEIMAGE_TAG}

ARG BASEIMAGE_NAME
ARG BASEIMAGE_TAG
ARG TARGET_ROS_DISTRO=noetic
ARG TARGET_ROS_PACKAGE_SUFFIX=desktop
ARG PYTHON_PACKAGE_PREFIX=python3


RUN echo Building ROS ${TARGET_ROS_DISTRO} image with ${BASEIMAGE_NAME}:${BASEIMAGE_TAG} and python ${PYTHON_PACKAGE_PREFIX}

# This is required, otherwise tzdata asks for input
ARG DEBIAN_FRONTEND=noninteractive

# Install other necessary packages
RUN apt-get update && \
    apt-get install -y gnupg2 curl tmux lsb-core vim wget cmake ${PYTHON_PACKAGE_PREFIX}-pip libpng16-16 libjpeg-turbo8 libtiff5 net-tools

# Install ROS-noetic
RUN sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list' && \
    curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | apt-key add - && \
    apt update && \
    apt install -y ros-${TARGET_ROS_DISTRO}-${TARGET_ROS_PACKAGE_SUFFIX} ${PYTHON_PACKAGE_PREFIX}-rosdep ${PYTHON_PACKAGE_PREFIX}-rosinstall ${PYTHON_PACKAGE_PREFIX}-rosinstall-generator ${PYTHON_PACKAGE_PREFIX}-wstool build-essential && \
    rosdep init && \
    rosdep update && \
    echo "source /opt/ros/${TARGET_ROS_DISTRO}/setup.bash" >> ~/.bashrc && \
    echo "/opt/ros/${TARGET_ROS_DISTRO}" > /etc/main_ros_workspace

# Intalling pyton-catkin
RUN sh -c 'echo "deb http://packages.ros.org/ros/ubuntu `lsb_release -sc` main" > /etc/apt/sources.list.d/ros-latest.list' && \
    wget http://packages.ros.org/ros.key -O - | apt-key add - && \
    apt-get update  && \
    apt-get install -y ${PYTHON_PACKAGE_PREFIX}-catkin-tools software-properties-common


ADD wait-for-ros-${TARGET_ROS_DISTRO}.sh /wait-for-ros.sh
ADD ./main_env.sh /main_env.sh
ADD ./ros_python3 /usr/bin/ros_python3
ADD ./ros_python /usr/bin/ros_python

RUN chmod +x wait-for-ros.sh && \
    chmod +x main_env.sh && \
    chmod +x /usr/bin/ros_python3 && \
    chmod +x /usr/bin/ros_python


#CMD ["tail", "-f", "/dev/null"]
