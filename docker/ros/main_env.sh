#!/bin/bash

_MAIN_ROS_WORKSPACE=$( cat /etc/main_ros_workspace )

$_MAIN_ROS_WORKSPACE/env.sh "$@"
unset _MAIN_ROS_WORKSPACE