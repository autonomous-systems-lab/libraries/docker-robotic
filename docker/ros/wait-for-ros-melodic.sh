#!/bin/sh


# Run "rostopic list" until it gives a result on stdout. That
# means the ros master is running. And also map stdout and stderr
# to "/dev/null" that it wont be displayed in the terminal
until /opt/ros/melodic/env.sh rostopic list > /dev/null 2>&1
do
  # Sleep for half a second and then try to reach ros master again
  sleep 0.5
done

# Execute all remaining arguments
exec "$@"

